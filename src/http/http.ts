export const tab: any = {
  1: 'da',
  2: 'wd',
  3: 'ew'
}

export const getTab = (caser: number) => {
  return tab[caser];
}