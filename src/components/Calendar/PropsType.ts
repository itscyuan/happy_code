import { type } from "os";
import { CSSProperties } from "react";

export type CalendarInstance = CalendarExpose;
export type CalendarDayInstance = {}
export type CalendarMonthInstance = {}
export type CalendarInstace = {}
export type CalendarExpose = {
  reset: (date?: Date | Date[]) => void
  scrollToDate: (targetDate: Date) => void
}
export type CalendarValue = Date | Date[]

export type CalendarType = 'single' | 'range' | 'multiple'

export type CalendarDayType =
  | ''
  | 'start'
  | 'start-end'
  | 'middle'
  | 'end'
  | 'selected'
  | 'multiple-middle'
  | 'multiple-selected'
  | 'disabled'
  | 'placeholder'
export type CalendarDayItem = {
  date?: Date
  text?: string | number
  type?: CalendarDayType
  topInfo?: string
  className?: unknown
  bottomInfo?: string
}

export type BottomDescribe = {
  start?: string;
  ['start-end']?: string;
  middle?: string;
  end?: string;
  selected?: string;
  ['multiple-middle']?: string;
  disabled?: string;
  placeholder?: string;
  ['multiple-selected']?: string;
  ''?: string;
}
export type Festival = {
  festivalDate: string | Date;
  festival: string;
}
// 日历props
export interface CalendarProps {
  className?: string;
  style?: CSSProperties;
  value?: CalendarValue;
  /**
   * 默认选择的值
   */
  defaultValue?: string | string[] | Date | Date[]; 
  onClose?: () => void;
  onClosed?: () => void;
  minDate?: Date;
  maxDate?: Date;
  /**
   * 类名的类型
   */
  type?: CalendarType;
  /**
   * 底部描述
   */
  bottomDescribe?: BottomDescribe;
  /**
   * 设置节日
   */
  festival?: Array<Festival>;
  /**
   * 确认日期选择
   */
  confirm?: (date: Date | Date[]) => void;
  title?: string;  // 日历选择
  isVisable?: boolean;
  cancel?: () => void;
}
// 日的props
export interface CalendarDayProps {
  item?: CalendarDayItem;
  onClick?: (item: CalendarDayItem) => void;
}
// 月的props
export interface CalendarMonthProps {
  calendarMonthData?: Array<CalendarDayProps>;
  onSwiperLeft?: (month: number) => void;
  onSwiperRight?: (month: number) => void;
  isShowRestDate?: boolean;
  onClickDay?: (item: CalendarDayItem) => void;
  calendarMonth: number;
  calendatYear: number;
  type?: CalendarType;
  defaultValue?: string | string[] | Date | Date[];
  currentDate?: Date | Date[];
  bottomDescribe?: BottomDescribe;
  festival?: Array<Festival>;
  minDate?: Date;
  maxDate?: Date;
}
// 头部
export interface CalendarHeadProps {
  title: string;
  month: number;
}