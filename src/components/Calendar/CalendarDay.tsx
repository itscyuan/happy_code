import { forwardRef, useEffect, useRef, useState } from "react";
import  { CalendarDayInstance, CalendarDayProps } from './PropsType';
import './style/calendarDay.scss';

export const CalendarDay = forwardRef<CalendarDayInstance, CalendarDayProps>(
  ({...props}, ref: any) => {
    const dayRef = useRef<HTMLDivElement>(null);

    const onClickHandle = () => {
      props.onClick && props.onClick(props.item || {});
    }

    return (
      <div 
        className={`item-day ${props.item?.type}`}
        onClick={onClickHandle}
      >
        <p
          className="top-info"
        >{props.item?.topInfo}</p>
        <p>{props.item?.text}</p>
        <p
          className="bottom-info"
        >{props.item?.bottomInfo}</p>
      </div>
    )
  }
)