
export function compareMonth(dateOne: Date, dateTwo: Date) {
  const yearOne = dateOne.getFullYear();
  const yearTwo = dateTwo.getFullYear();

  if(yearOne === yearTwo) {
    const monthOne = dateOne.getMonth();
    const monthTwo = dateTwo.getMonth();
    
    return monthOne === monthTwo ? 0 : monthOne > monthTwo ? 1 : -1;
  }
  return yearOne > yearTwo ? 1 : -1;
}

export const cloneDate = (date: Date) => new Date(date);

export const getDayByOffset = (date: Date, number: number) => {
  const clonedDate = cloneDate(date);
  clonedDate.setDate(clonedDate.getDate() + number);
  return clonedDate; 
}
export const getPrevDay = (date: Date) => getDayByOffset(date, -1);
export const getNextDay = (date: Date) => getDayByOffset(date, -1);

export function compareDate(dateOne: Date, dateTwo: Date) {
  let compareMonthResult = compareMonth(dateOne, dateTwo);
  
  if(compareMonthResult === 0) {
    const dayOne = dateOne.getDate();
    const dayTwo = dateTwo.getDate();

    return dayOne === dayTwo ? 0 : dayOne > dayTwo ? 1 : -1;
  }
  return compareMonthResult;
}

export function getDay() {
  const date = new Date();
  let today = date.setHours(0, 0, 0, 0);
  return new Date(today);
}
// 获取第几个月的最后一天
export function getMonthLastDay(year: number, month: number) {
  const date = new Date(year, month + 1, 0);
  return date.getDate();
}
// 获取前一个月最后一天是星期几
export function getPrevMonthWeekDay(year: number, month: number) {
  const date = new Date(year, month, 0);
  return date.getDay();
}
// 获取年月日
export function getDate(date: Date | string = new Date()) {
  const currentDate = typeof date === 'string' ? new Date(date) : date;

  return [
    currentDate.getFullYear(),
    currentDate.getMonth(),
    currentDate.getDate()
  ]
}
// 格式化日期
export function formatDate(date: Array<number>) {
  date.map(item => item < 10 ? `0${item}` : item);
}
//  切割数据
export function sliceData(data: Array<any>, start: number = 0, end?: number) {
  return end !== undefined ? data.slice(start, end) : data.slice(start);
} 
// 比较两个时间是否相等
export function compareMs(date_one: string | Date, date_two: string | Date) {
  if(typeof date_one === 'string' && typeof date_two === 'string') {
    return Date.parse(date_one) === Date.parse(date_two);
  } 
  if(typeof date_one === 'object' &&  typeof date_two === 'object') {
    return date_one.getTime() === date_two.getTime();
  }
}

export const formatDateStrToDate = (str?: string | Date) => {
  if(!str) return;
  if(typeof str === 'string') return new Date(str);
  return str;
}