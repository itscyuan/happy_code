import React, { forwardRef, useEffect, useMemo, useState } from "react";
import './style/calendar.scss';
import { CalendarInstance, CalendarProps, CalendarDayProps, CalendarDayItem } from './PropsType'
import { CalendarMonth } from "./CalendarMonth";
import { CalendarHead } from "./CalendarHead";
import { Swiper, SwiperSlide } from "swiper/react";
import {
  getDate,
  compareDate,
  formatDateStrToDate
} from "./utils";
import { Popup } from "../Popup/Popup";

export const Calendar = forwardRef<CalendarInstance, CalendarProps>(
  ({ className, style, ...props }, ref) => {
    // 年
    const [year, ] = useState<number>(getDate()[0]);
    // 月
    const [monthes, setMonthes] = useState<number>(getDate()[1]);

    const [value, setValue] = useState<Date | Date[]>();

    // const [isVisable, setIsVisable] = useState<boolean>(false);
    useEffect(() => {
      handleDefaultData();
    }, [props.defaultValue]);

    // 处理默认值
    const handleDefaultData = () => {
      let temp = props.defaultValue;
      if(Array.isArray(temp)) {
        temp = temp.map((item: any) => typeof item === 'string' ? formatDateStrToDate(item) : item);
      } else {
        temp = formatDateStrToDate(temp as string);
      }
      props.type === 'single' ? setValue(temp as Date) : setValue(temp as Date[]);
    }
    // 所有月份
    const totalMonthes = useMemo(() => {
      let totalMonthesArr: Array<number> = [];
      for(let i = 0; i < 12; i++) {
        totalMonthesArr.push(i);
      }
      return totalMonthesArr; 
    }, [year]);

    // 向右滑动触发日期➕1
    const onSlideNextTransitionStartHandle = () => {
      let index = monthes;
      if(index <= 11) {
        setMonthes(index + 1);
      } else {
        setMonthes(1);
      }
    }
    // 向右触发日期-1
    const onSlidePrevTransitionEndHandle = () => {
      let index = monthes;
      if(index > 1) {
        setMonthes(index - 1);
      } else {
        setMonthes(12);
      }
    }
    // 处理点击事件
    const getDateHandle = (item: CalendarDayItem) => {
      if(props.type === 'multiple') {
        let temp: any = value || [];
        item.date && temp.push(item.date);
        setValue([...temp]);
      }
      if(props.type === 'range') {
        let temp: any = value || [];
        if(temp.length > 1) {
          temp = [];
        }
        item.date && temp.push(item.date);
        const [start, end] = temp;
        if(end) {
          if(compareDate(start, end) === 1) {
            temp = temp.slice(1, 2);
          }
        }
        setValue([...temp]);
      }
      if(props.type === 'single') {
        setValue(item.date);
      }
    }
    // 确认按钮
    const confirm = () => {
      let temp = value || [];
      props.confirm && props.confirm(temp);
    }

    return (
      <Popup 
        visable={props.isVisable}
        onClick={props.cancel}
        direction='bottom'
        visableHeight={'85%'}
      >
      <div className="calendar-bottom">
        <CalendarHead 
          title={props.title || '日历'} 
          month={monthes}
        />
        <div className="calendar-content">
          <Swiper
            initialSlide={monthes}
            onSlideNextTransitionEnd={onSlideNextTransitionStartHandle}
            onSlidePrevTransitionEnd={onSlidePrevTransitionEndHandle}
            loop
          >
            {
              totalMonthes.map((item: number, index: number) => {
                return (
                  <SwiperSlide key={item}>
                    <CalendarMonth
                      calendarMonth={item}
                      calendatYear={year}
                      {...props}
                      onClickDay={getDateHandle}
                      currentDate={value}
                      bottomDescribe={props.bottomDescribe}
                      maxDate={formatDateStrToDate(props.maxDate)}
                      minDate={formatDateStrToDate(props.minDate)}
                    />
                  </SwiperSlide>
                )
              })
            }
          </Swiper>
        </div>
        <div className="calendar-confirm-bg">
          <button 
            className={`calendar-confirm canSelect`}
            onClick={confirm}
          >确认</button>
        </div>
      </div>
      </Popup>

    )
  }
)