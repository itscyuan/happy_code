import { forwardRef, useEffect, useMemo, useState } from "react";
import './style/calendarHead.scss';
import { CalendarHeadProps, CalendarInstace } from './PropsType';
import { getDate } from "./utils";

export const CalendarHead = forwardRef<CalendarInstace ,CalendarHeadProps>(
  ({...props}, ref) => {
    const [weekDay, ] = useState<Array<string>>(['日', '一', '二', '三', '四', '五', '六']);

    return (
      <header 
        className="calendar-head"
      >
        <span className="calendar-head-title">{props.title || '日历标题'}</span>
        <div className="calendar-head-sub">{`${getDate()[0]} 年 ${props.month} 月`}</div>
        <ul className="caledar-head-week">
          {
            weekDay.map((item: string, index: number) => {
              return (
                <li 
                  key={index + 1}
                  className='caledar-head-item'
                >{item}</li>
              )
            })
          }
        </ul>
      </header>
    )
  }
)