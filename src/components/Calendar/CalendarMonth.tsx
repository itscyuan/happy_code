import { forwardRef, memo, ReactNode, useEffect, useMemo, useRef, useState } from "react";
import { CalendarDayProps, CalendarDayItem, CalendarDayType, BottomDescribe, Festival } from "./PropsType";
import { CalendarDay } from "./CalendarDay";
import { CalendarMonthInstance, CalendarMonthProps } from './PropsType';
import './style/calnedarMonth.scss';
import 'swiper/css';
import { 
  getPrevMonthWeekDay,
  getMonthLastDay,
  compareDate,
  formatDateStrToDate
 } from "./utils";


export const CalendarMonth = forwardRef<CalendarMonthInstance, CalendarMonthProps>(
  ({...props}, refs) => {

    const totalDays = useMemo(
      () => {
        return getMonthLastDay(props.calendatYear, props.calendarMonth);
      }
    , [props.calendarMonth, props.calendatYear])

    const offset = useMemo(
      () => {
        return getPrevMonthWeekDay(props.calendatYear, props.calendarMonth) % 7;
      },
      [props.calendarMonth, props.calendatYear]
    ) 
 
    const multipleNameHand = (currentDate: Date[], day: Date): CalendarDayType => {
      let isSelected = (currentDate as Date[]).some(item => compareDate(item, day) === 0);
      
      if(isSelected) {
        return 'selected';
      }
      return ''
    }
    const rangeNameHand = (currentDate: Date[], day: Date): CalendarDayType => {
      const [start, end] = currentDate;
      if(compareDate(start, day) === 0) return 'start';

      if(end) {
        if(compareDate(end, day) === 0) return 'end';
        if(
          compareDate(start, day) === -1 
          && compareDate(end, day) === 1
        ) {
          return 'middle';
        }
      }
      return '';
    }

    // 添加类名
    const getDayType = (day: Date): CalendarDayType => {
      const { type, currentDate } = props;

      if(!currentDate) return '';

      if(props.maxDate && props.minDate) {
        if(compareDate(day, props.minDate) === -1 || compareDate(day, props.maxDate) === 1) {
          return 'disabled';
        }
      }
      if(Array.isArray(currentDate)) {
        if(type === 'multiple') {
          return multipleNameHand(currentDate, day);
        }
        if(type === 'range') {
          return rangeNameHand(currentDate, day);
        }
      } else {
        return compareDate(day, currentDate) === 0 ? 'selected' : ''
      }
      return ''
    }
    // 设置选择底部按钮
    const setButtomInfo = (type: CalendarDayType) => {
      const { start = '开始', end = '结束', middle, disabled, selected, placeholder } = props.bottomDescribe || {};
      const data: BottomDescribe = {
        start,
        end,
        middle,
        disabled,
        selected,
        placeholder
      }
      return data[type];
    }
    // 设置顶部标签
    const setTopInfo = (date: Date) => {
      if(!props.festival) return '';
      let festivalData: Array<Festival> = props.festival.map((item: Festival) => typeof item.festivalDate === 'string' ? {festivalDate: formatDateStrToDate(item.festivalDate) || '', festival: item.festival} : item);
      return festivalData.filter((item: Festival) => compareDate(date, item.festivalDate as Date) === 0)[0]?.festival;
    }
    
    const days = useMemo(() => {
      const { calendatYear, calendarMonth } = props;
      let offsetDays = Array(offset + 1).fill({type: 'placeholder'});
      const internalDays: CalendarDayItem[] = [...offsetDays];
      
      for(let i = 1; i <= totalDays; i++) {
        let date = new Date(calendatYear, calendarMonth, i);
        let type = getDayType(date);
        let bottomInfo = setButtomInfo(type);
        let topInfo = setTopInfo(date);
        let config: CalendarDayItem = {
          text: i,
          type,
          date,
          bottomInfo,
          topInfo
        }
        internalDays.push(config);
      }

      return internalDays;
    }, [totalDays, getDayType]);

    const renderMonth = (node: CalendarDayItem, index: number): ReactNode => {
      return (
        <CalendarDay
          key={index + 1}
          item={node}
          onClick={props.onClickDay}
        />
      )
    }
    return (
      <>
        <div className="month-day">
          {
            days?.map(renderMonth)
          }
        </div>
      </>
    )
  }
)