import { FC, useEffect, useMemo, useRef, useState } from 'react';
import { OverLayProps } from './PropsType';
import './style/overlay.scss';

export const OverLay: FC<OverLayProps> = ( props: any ) => {

  const [isVisable, setIsVisable] = useState<boolean>(false);

  const [transition, setTransition] = useState<boolean>(false);

  const ref = useRef<any>();

  const style = useMemo(() => {
    let zIndex = props.zIndex ? props.zIndex : undefined;
    let transition = `all ${props.duration || .3}s`;
    return {
      zIndex,
      transition,
      ...props.coustomStyle
    }
  }, [props.zIndex]);

  useEffect(() => {
    if(props.isVisable) {
      setTransition(props.isVisable);
      ref.current = setTimeout(() => {
        setIsVisable(props.isVisable);
      }, 100);
    } else {
      setIsVisable(props.isVisable);
      ref.current = setTimeout(() => {
        setTransition(props.isVisable);
      }, 300);
    }
    return () => {
      clearTimeout(ref.current);
    }
  }, [props.isVisable]);

  const onClick = () => {
    props.close && props.close();
    props.onClick && props.onClick(!props.isVisable);
  }

  const renderLay = () => {
    if(!transition) {
      return null;
    }

    return (
      <div 
        className='over-lay'
        style={{...style, ...{opacity: isVisable ? '.5': '0'}}}
        onClick={onClick}
      >
      </div>
    )
  }

  return (
    <>
      {renderLay()}
    </>
  )
}