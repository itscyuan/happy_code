import { CSSProperties } from "react";

export interface OverLayInstance {

}
export interface OverLayProps {
  zIndex?: number;
  isVisable?: boolean;
  duration?: number;
  coustomStyle?: CSSProperties;
  close?: () => void;
  onClick?: (bool: boolean) => void;
}