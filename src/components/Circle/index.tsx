import React, { FC } from "react";
import './index.scss';

interface IProps {

}
export const Circle: FC<IProps> = (props: any) => {

  return (
    <div className="circle">
      <div className="real-circle">
        <div className="inner-circle"></div>
        <div className="inner-pointer"></div>
      </div>
    </div>
  )
}