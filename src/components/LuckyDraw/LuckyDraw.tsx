import { FC, ReactNode, useMemo, useRef, useState } from 'react';
import "./style/luckyDraw.scss";
import { LuckyDrawProps, PrizeDescription, PrizeDescriptionDetail } from './PropsType'; 
import { LuckyDrawItem } from './LuckyDrawItem';
import { Toast } from '../Toast/Toast';

export const LuckyDraw: FC<LuckyDrawProps> = ( props: any ) => {

  const [activeIndex, setActiveIndex] = useState<number>(0);

  const TimeRef = useRef<any>(null);

  const luckyDrawData: PrizeDescriptionDetail[] = useMemo(() => {
    let initData: PrizeDescriptionDetail[] = []
    let arr = [
      {prizeOrder: 0},
      {prizeOrder: 1},
      {prizeOrder: 2},
      {prizeOrder: 7},
      {prizeOrder: 8},
      {prizeOrder: 3},
      {prizeOrder: 6},
      {prizeOrder: 5},
      {prizeOrder: 4}
    ]
    for(let i = 0; i < arr.length; i++) {
      initData.push({...arr[i], ...props.prizeDescription[i]});
    }
    return initData;
  }, [props.prizeDescription]);

  const createRandom = (m: number, n: number): number => {
    return Number( (Math.random() * (m - n + 1) + n).toFixed(0));
  }

  // 0 1 2 5 8 7 6 3
  const onClick = () => {
    if(TimeRef.current) return;
    let count = 0;
    let index = 0;
    let time = 100;
    let random = createRandom(0, 100);
    function fn() {
      index = index > 6 ? 0 : index + 1;
      setActiveIndex(index);
      clearInterval(TimeRef.current);
      time += 20;
      count++;
      if(count > 30) {
        let curItem: PrizeDescriptionDetail[] = luckyDrawData.filter(item => item.prizeOrder === index);
        const [min, max] = curItem[0].prizeProb || [];
        if(min <= random && max >= random) {
          clearInterval(TimeRef.current);
          TimeRef.current = null;
          return;
        }
      }
      TimeRef.current = setInterval(fn, time);
    }
    TimeRef.current = setInterval(fn, time);
  }

  const renderItem = (list: PrizeDescriptionDetail[]): ReactNode => {
    return list.map((item: PrizeDescriptionDetail, index: number) => {
      return (
        <LuckyDrawItem 
          key={item.id} 
          item={item} 
          index={index}
          activeIndex={activeIndex}
          onClick={onClick}
        />
      )
    })
  }

  return (
    <div className='lucky-draw'>
      {renderItem(luckyDrawData)}
    </div>
  )
} 
LuckyDraw.defaultProps = {
  prizeDescription: [
    { desc: '1', id: '0' , prizeProb: [0, 10]},
    { desc: '2', id: '1' , prizeProb: [11, 20]},
    { desc: '3', id: '2' , prizeProb: [21, 30]},
    { desc: '4', id: '3' , prizeProb: [31, 40]},
    { desc: 'render', id: '4' },
    { desc: '5', id: '5' , prizeProb: [41, 50]},
    { desc: '6', id: '6' , prizeProb: [51, 60]},
    { desc: '7', id: '7' , prizeProb: [61, 70]},
    { desc: '8', id: '8' , prizeProb: [71, 100]}
  ]
}