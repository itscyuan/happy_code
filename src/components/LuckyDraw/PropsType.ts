export interface LuckyDrawProps {
  prizeDescription?: PrizeDescription[];
}

export interface LuckyDrawItemProps {
  item: PrizeDescription;
  index: number;
  activeIndex: number;
  onClick: () => void;
}

export interface PrizeDescription {
  id: string;
  desc: string;
  prizeProb?: number[];
}
export interface PrizeDescriptionDetail extends PrizeDescription{
  prizeOrder: number;
}