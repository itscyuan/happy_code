import { FC } from 'react';
import './style/luckyDrawItem.scss';
import { LuckyDrawItemProps } from './PropsType';

export const LuckyDrawItem: FC<LuckyDrawItemProps> = ( props: any) => {

  const { item, index, activeIndex } = props || {};

  const onClicked = () => index === 4 && props.onClick && props.onClick();

  return (
    <div 
      className={item.prizeOrder === activeIndex ? 'lucky-draw-item-select' : 'lucky-draw-item-unselect'}
      onClick={onClicked}
    >
      <p className='lucky-draw-item-desc'>{item.desc}</p>
    </div>
  )
}