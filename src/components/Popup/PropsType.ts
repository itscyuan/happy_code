export interface PopupProps {
  direction?: Direction;
  visableHeight?: number | string;
  visable?: boolean;
  onClick?: () => void;
  children?: any;
}

export type Direction = 'left' | 'top' | 'right' | 'bottom' | 'center';