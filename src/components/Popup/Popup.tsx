import React, { FC, useEffect, useMemo, useState } from "react";
import './style/popup.scss';
import { PopupProps, Direction } from './PropsType';
import { OverLay } from "../OverLay/OverLay";
import { numToPx } from '../../utils/base';

export const Popup: FC<PopupProps> = props => {

  const [isVisable, setIsVisable] = useState<boolean>(false);

  useEffect(() => {
    setIsVisable(props.visable || false);
  }, [props.visable]);

  const onClicked = () => {
    setIsVisable(false);
    props.onClick && props.onClick();
  }
  const renderLay = () => {
    return (
      <OverLay 
        zIndex={10}
        isVisable={isVisable}
        onClick={onClicked}
      />
    )
  }
  
  const contentCofig = (type: Direction) => {
    const data: any = {
      left: {
        height: '100%',
        width: numToPx(props.visableHeight || '70%'),
        bottom: 0,
        left: 0
      },
      right: {
        height: '100%',
        width: numToPx(props.visableHeight || '70%'),
        bottom: 0,
        right: 0
      },
      bottom: {
        height: numToPx(props.visableHeight || '70%'),
        borderRadius: '10px 10px 0 0',
        bottom: 0,
        left: 0,
        width: "100%"
      },
      top: {
        height: numToPx(props.visableHeight || '40%'),
        borderRadius: '0 0 10px 10px',
        top: 0,
        left: 0,
        width: "100%"
      }
    }
    return data[type];
  }

  const style: any = useMemo(() => {
    return contentCofig(props.direction || 'bottom');
  }, [props.visableHeight, props.direction]);

  const moveConfig = (direction: Direction) => {
    let data: any = {
      left: isVisable ? `translateX(0)` : `translateX(-100%)`,
      right: isVisable ? `translateX(0)` : `translateX(100%)`,
      bottom: isVisable ? `translateY(0)` : `translateY(100%)`,
      top: isVisable ? `translateY(0)` : `translateY(-100%)`
    }
    return data[direction];
  }
 
  const move = useMemo(() => {
    return moveConfig(props.direction || 'bottom');
  }, [isVisable]);

  const renderContent = () => {
    return (
      <div 
        className="content"
        style={{...style, transform: move}}
      >
        {props?.children}
      </div>
    )
  }

  return (
    <>
      {renderLay()}
      {renderContent()}
    </>
  )
}