export type type = 'success' | 'error' | 'warning';

export interface IProps {
  /**
   * 控制显示隐藏
   */
  isShowDialog?: boolean;
  /**
   * 提示文本
   */
  tipText?: string;
  /**
   * 提示类型
   */
  type?: type;
  /**
   * 延迟消失时间 默认为3秒自动消失 单位是毫秒
   */
  timerOut?: number;
  /**
   * 弹窗隐藏后的回调
   */
  cancelCallback?: () => void;
}