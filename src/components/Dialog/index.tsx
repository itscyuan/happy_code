import React, { FC, memo, useEffect, useRef, useState } from "react";
import './index.scss';
import { AiFillCheckCircle, AiFillCloseCircle, AiFillExclamationCircle } from "react-icons/ai";
import { IProps } from './index.d';


const currentType: any = {
  success: <AiFillCheckCircle color="green" />,
  error: <AiFillCloseCircle color="red" />,
  warning: <AiFillExclamationCircle color="orange" />
}

export const Dialog: FC<IProps> = memo((props: any) => {

  const { 
    isShowDialog, 
    tipText = "我是一个mesage", 
    type = 'success',
    timerOut = 3000,
    cancel,
    cancelCallback
  } = props;

  const refHeight = useRef<any>();
  // 控制显示和隐藏
  const [isShowDialoger, setIsShowDialoger] = useState<boolean>();

  // 定义状态保存延迟器
  const [currentTimeOut, setCurrentTimeOut] = useState<NodeJS.Timeout>();

  useEffect(() => {
    // 初始化控制提示显示隐藏
    setIsShowDialoger(isShowDialog);
    isShowDialog && hideShowDialog(false, timerOut);
  }, [isShowDialog]);

  useEffect(() => {
    console.log(refHeight.current.offsetHeight);
    debugger
  }, [refHeight.current.offsetHeight])
  /**
   * 根据type 返回对应的提示类型
   * @param type 图标类型
   * @returns 需要渲染的图标类型
   */
  const getIcon = (type: string) => {
    return currentType[type];
  }
  /**
   * 处理鼠标划入不隐藏提示
   */
  const handlerMouseEnter = () => {
    if(currentTimeOut) {
      clearTimeout(currentTimeOut);
    }
  }
  /**
   * 处理鼠标划出隐藏提示
   */
  const handlerMouseLeave = () => {
    hideShowDialog(false, timerOut);
  }

  /**
   * 延迟隐藏提示
   */
  const hideShowDialog = (isShow: boolean, timerOut: number = 3000) => {
    if(currentTimeOut) {
      clearTimeout(currentTimeOut);
    }
    let timer = setTimeout(() => {
      setIsShowDialoger(isShow);
      cancelCallback && cancelCallback()
    }, timerOut);
    setCurrentTimeOut(timer);
  }
  return (
    <div className="dialog_background">
      <div 
        className="dialog"
        style={{
          transform: isShowDialoger ? 'translateY(100%)' :  'translateY(-100%)',
          opacity: isShowDialoger ? '1' : '0'
        }}
        onMouseEnter={handlerMouseEnter}
        onMouseLeave={handlerMouseLeave}
        ref={refHeight}
      >
        <p className="dialog_content">
          { getIcon(type) }
          <span className="dialog_content_text">{tipText}</span>
        </p>
      </div>
    </div>
  )
})