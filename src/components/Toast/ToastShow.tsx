import { CSSProperties, useMemo, FC } from "react";
import './style/toastshow.scss';
import { ToastShowProps } from './PropsType';

export const ToastShow: FC<ToastShowProps> = props => {

  const style = useMemo(() => {
    return {
      position: 'fixed',
      left: '50%',
      transform: 'translateX(-50%)',
      top: '30px',
      // opacity: props.isVisable ? '.3' : '0'
    } as CSSProperties;
  }, []);

  return (
    <div 
      className="toast-show"
      style={style}
    >
      <span className="toast-span">{props.option}</span>
    </div>
  )
}