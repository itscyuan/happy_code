import React, { FC, forwardRef, useEffect, useImperativeHandle, useRef, useState } from "react";
import './style/toast.scss';
import { ToastProps, Method, ToastInstance } from './PropsType';
import { ToastShow } from './ToastShow';
import { createRoot } from "react-dom/client";

let notificationInstance: {addNotice: () => void; removeNotice: () => void};
interface Notice {
  key: string | number;
  message : string;
}

export const Toast: any = forwardRef<ToastInstance ,ToastProps>(
  ({...props}, ref) => {

    const [isVisable, setIsVisable] = useState<boolean>(false);
  
    const [notices, setNotices] = useState<any []>([]);

    const timeOutRef = useRef<any>();

    function getUnique() {
      return `${Math.random()}${notices.length}`
    }

    useEffect(() => {
      return () => {
        clearTimeout(timeOutRef.current);
      }
    }, []);

    useImperativeHandle(ref, () => ({
      isVisable,
      detroyElement,
      addNotice
    }))

    const addNotice = (option: any) => {
      const key = getUnique();
      if(notices.every((item: Notice) => item.key !== key)) {
        let temp: Notice [] = [{message: option, key}]
        setNotices(temp);
      }
      detroyElement(key);
    }
    const detroyElement = (key: string | number) => {
      timeOutRef.current = setTimeout(() => {
        setNotices([]);
      }, 3000)
    }

    return (
      <>
        {
          notices.map((item: Notice) => {
            return (
              <ToastShow 
                option={item.message}
                key={item.key}
                // isVisable={isVisable}
              />
            )
          })
        }
      </>
    )
  }
)

async function notification(type: any, option: any): Promise<any> {
  const element = document.createElement('div');
  document.body.appendChild(element);

  const ref: any = React.createRef();

  const elementRoot = createRoot(element);

  await elementRoot.render(
    <Toast 
      type={type} 
      option={option}
      ref={ref}
    />
  )
  return {
    addNotice() {
      return ref.current.addNotice(option)
    },
    removeNotice() {
      elementRoot.unmount();
      document.body.removeChild(element);
    }
  }
}
const createMethod = (type: any) => async (option: any) => {
  if(!notificationInstance) notificationInstance = await notification(type, option);
  notificationInstance.addNotice();
}


;['show', 'alert'].forEach((method) => {
  Toast[method] = createMethod(method);
});



