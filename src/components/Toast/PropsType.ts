export interface ToastProps {
  type: 'show' | 'alert',
  option: string;
}
export interface ToastInstance {

}

export interface ToastShowProps {
  // type: 'show' | 'alert',
  option: string;
  // isVisable: boolean;
}
export interface instanceToastNotice {

}

export type Method = {
  show: (option: any) => void;
  alert: (option: any) => void;
}