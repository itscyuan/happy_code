import { FC, useMemo, useState } from 'react';
import './style/keywordssearch.scss';
import { KeyWordsSearchProps, SearchList } from './PropsType';

export const KeyWordsSearch: FC<KeyWordsSearchProps> = (props: KeyWordsSearchProps) => {

  const { isVisable, searchList, onClick } = props;

  const [currentIndex, setCurrentIndex] = useState<number>(-1);

  const mouseEnterHandle = (index: number) => {
    setCurrentIndex(index);
  }

  const clickHandle = (item: SearchList) => {
    onClick && onClick(item);
  }

  if(!(searchList.length > 0)) return null;

  return (
    <ul 
      className='key-words-search'
      onMouseLeave={() => setCurrentIndex(-1)}
      style={{
        maxHeight: isVisable ? '200px' : '0px',
        borderColor: isVisable ? 'var(--theme-color-one)' : 'transparent'
      }}
    >
      {
        searchList.map((item: SearchList, index: number) => {
          return (
            <li
              className={index === currentIndex ? 'key-words-item-select' : 'key-words-item-unselect' }
              key={item.id}
              onMouseEnter={() => {mouseEnterHandle(index)}}
              onClick={() => clickHandle(item)}
            >{item.content}</li>
          )
        })
      }
    </ul>
  )
}