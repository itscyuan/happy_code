import { FC, useMemo, useState } from 'react';
import { SearchProps, SearchList } from './PropsType';
import './style/search.scss';
import { debounce } from '../../utils/base';
import { KeyWordsSearch } from './KeyWordsSearch';

export const Search: FC<SearchProps> = ( props: SearchProps ) => {

  const { placeholder, search, searchBtn } = props;

  const [borderColor, setBorderColor] = useState<boolean>(false);

  const [value, setValue] = useState<string>('');

  const [isVisableList, setIsVisableList] = useState<boolean>(false);

  const onFocusHandle = () => {
    setBorderColor(true);
    setIsVisableList(true);
  }

  const onBlurHandle = () => {
    setBorderColor(false);
    setIsVisableList(false);
  }

  const searchHandle = () => {
    search && search(value);
  }

  const inputHandle = (e: any) => {
    let targetValue = e.target.value;
    setValue(targetValue);    
  }

  const keyWordsHandle = (item: SearchList) => {
    setValue(item.content);
  }
  
  const fileter = useMemo(() => {
    return props.listData?.filter((item: SearchList) => {
      return item.content.indexOf(value) === 0;
    });
  }, [value]);

  return (
    <>
      <div 
        className='search'
        style={{
          borderColor: !borderColor ? '' : 'var(--theme-color-one)'
        }}
      >
        <input 
          type="text" 
          placeholder={placeholder}
          className='search-input'
          onFocus={onFocusHandle}
          onBlur={onBlurHandle}
          value={value}
          onInput={inputHandle}
        />
        <p 
          className='search-btn'
          onClick={searchHandle}
        >{searchBtn}</p>
      </div>
      <KeyWordsSearch 
        searchList={fileter || []} 
        isVisable={isVisableList}
        onClick={keyWordsHandle}
      />
    </>
  )
}
Search.defaultProps = {
  placeholder: '请输入搜索内容',
  searchBtn: '搜索'
}