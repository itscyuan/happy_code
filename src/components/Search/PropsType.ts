
export interface SearchProps {
  placeholder?: string;
  searchBtn?: string;
  search?: (content: string) => void;
  listData?: SearchList[];
}

export type SearchList = {
  id: string;
  content: string;
}
export interface KeyWordsSearchProps {
  searchList: SearchList[];
  isVisable: boolean;
  onClick?: (item: SearchList) => void;
}