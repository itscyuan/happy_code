import React, { FC, useEffect, useState } from 'react';
// import './index.scss';
interface Props {
  dater?: number;
}
export const Counter: FC<Props> = ( props: any ) => {

  const [counter, setCounter] = useState(0);

  useEffect(() => {
    // console.log("effect执行");
  }, [props.dater]);
  
  return (
    <div>
      <h1>{counter}</h1>
      <h2>{props.dater}</h2>
      <div onClick={() => { setCounter(counter + 1)}}>add</div>
      <div onClick={() => { setCounter(counter - 1)}}>sub</div>
    </div>
  )
}