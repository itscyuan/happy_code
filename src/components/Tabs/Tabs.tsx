import React, { CSSProperties, FC, useEffect, useMemo, useRef, useState } from "react";
import './style/tabs.scss';
import { TabsProps, tabsItem, itemConfig } from './Propstype';
import { TabsItem } from "./TabItem";
import useRefs from '../../hooks/use-refs';

export const Tabs: FC<TabsProps> = props => {
  // 保存当前选择的tab-item
  const [curSelect, setCurSelect] = useState<any>(
    () => {
      if(props.tabsData?.length > 0) {
        return props.tabsData[0];
      }
    }
  );
  // 保存当前的则的索引
  const [curIndex, setCurIndex] = useState<number>(0);

  const [childRefs, setChildRefs] = useRefs();
  
  // 初始化进度条
  const [update, setUpadate] = useState<boolean>(false);

  const root = useRef<any>();

  const width = useMemo(() => {
    if(props.scrollLeft) {
      return 50 * props.tabsData.length;
    }
    return undefined;
  }, [props.tabsData]);

  // init
  useEffect(() => {
    setUpadate(true);
  }, []);

  // 滑块偏移距离
  const defaultOffset = useMemo(() => {
    let offset = childRefs?.[curIndex];  
    return offset?.offsetLeft + offset?.offsetWidth / 2;
  }, [root.current, curIndex, childRefs, update]);

  // 点击事件
  const tabsOnClick = (item: tabsItem, index: number) => {
    if(item.config === 'disabled') return;
    props.scrollLeft && setScrollTo(root.current, index);
    props.onClicked && props.onClicked(item);
    setCurIndex(index);
    setCurSelect(item);
  }

  function animate(obj: any, target: number, callback?: () => {}) {
    if(obj.timer) clearInterval(obj.timer);

    obj.timer = setInterval(() => {
   
      let step = (target - obj.scrollLeft) / 10;
      
      step = step > 0 ? Math.ceil(step) : Math.floor(step);
  
      if(obj.scrollLeft === target || target < 0) {
        clearInterval(obj.timer);
      }
      obj.scrollLeft = obj.scrollLeft + step;

    }, 10);
  }

  const setScrollTo = (scrollElement: HTMLElement, index: number) => {
    let offset = childRefs?.[index];  
    // 屏幕宽度
    const SCREEN_WIDTH = window.screen.width;
    // 偏移距离
    let offsetScroll = (offset.offsetLeft - scrollElement.scrollLeft) - (SCREEN_WIDTH / 2);
    
    let target = scrollElement.scrollLeft + offsetScroll;
    // 判断边界条件                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    if(target > scrollElement.scrollWidth - SCREEN_WIDTH) {
      target = scrollElement.scrollWidth - SCREEN_WIDTH;
    }
    if(target < 0) target = 0;

    animate(scrollElement, Math.round(target));
  }

  // 下划线的样式
  const tabSlideStyle = useMemo(() => {

    const { lineColor, lineWidth, lineDuration } = props;
    const slideStyle = {
      width: lineWidth || "30px",
      backgroundColor: lineColor || "#ee0a24",
      transitionDuration: `all ${ lineDuration || .3 }ms`,
    } as CSSProperties;

    if(defaultOffset) {
      slideStyle.transform = `translateX(${defaultOffset}px) translateX(-50%)`;
    } 
    return slideStyle;
  }, [curIndex, defaultOffset]);

  // set tab config;
  const setConfig = (item: tabsItem): itemConfig => {
    if(item.disable === true) return 'disabled';

    if(curSelect?.id === item.id) return 'select';

    return 'unselect';
  }

  // set tab
  const tabsChangeSetConfig = useMemo(() => {
    let data = props.tabsData;
    for(let i = 0; i < data.length; i++) {
      data[i].config = setConfig(data[i]);
    }
    return data;
  }, [curSelect]);

  const renderTabItem = (item: tabsItem, index: number) => {
    return (
      <TabsItem
        key={item.id}
        tabItem={item}
        onClick={tabsOnClick}
        index={index}
        ref={setChildRefs(index)}
      />
    )
  }

  const renderTabSlide = () => {
    return (
      <div 
        style={tabSlideStyle}
        className="slide"
      ></div>
    )
  }
  return (
    <div className="tabs" 
      ref={root}
    >
      <div className="tabs-list"
        style={{width}}
      >
        {
          props.tabsData 
          &&
          tabsChangeSetConfig.map(renderTabItem)
        }
      </div>
      {
        renderTabSlide()
      }
    </div>
  )
}