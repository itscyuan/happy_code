export type itemConfig =  'select' | 'unselect' | 'disabled';
export type tabsItem = {
  name: string;
  id: string;
  disable?: boolean;
  config?: itemConfig;
}

export interface TabsProps {
  tabsData: Array<tabsItem>;
  /**
   * 是否开启滚动体条
   */
  scrollLeft?: boolean;
  /**
   * 滑块颜色
   */
  lineColor?: string;
  /**
   * 滑块宽度
   */
  lineWidth?: string;
  /**
   * 滑块过度速度
   */
  lineDuration?: number;
  /**
   * 点击获取当前tab
   */
  onClicked?: (data: tabsItem) => void;
}

export interface TabsItemProps {
  tabItem: tabsItem;
  width?: number;
  onClick: (item: tabsItem, index: number) => void;
  index: number;
}
export interface TabIteminstance {

}