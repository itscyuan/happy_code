import React, { FC, useRef, forwardRef } from "react";
import './style/tabItem.scss';
import { TabsItemProps, TabIteminstance } from './Propstype';

export const TabsItem = forwardRef<TabIteminstance, TabsItemProps>(({...props}, ref: any) => {

  const { width, onClick, tabItem, index } = props; 

  return (
    <div 
      className={`tab-item`}
      style={{width}}
      onClick={() => onClick && onClick(tabItem, index)}
      ref={ref}
    >
      <span 
        className={`item-txt ${tabItem.config}`}  
      >{props.tabItem.name}</span>
    </div>
  )
})