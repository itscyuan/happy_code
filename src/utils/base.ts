
export function isFunction(val: unknown): val is Function {
  return typeof val === 'function';
}

export function numToPx(num: number | string) {
  return typeof num === 'string' ? num : `${num}px`;
}

export function debounce(fnc: Function, time: number) {
  let timer: null | NodeJS.Timeout = null;
  return function(args: any) {
    if(timer) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fnc.call(null,  ...args);
    }, time);
  }
}