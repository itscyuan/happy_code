/**
 * 获取某年某月一号是星期几
 * @param year 
 * @param month 
 * @returns 
 */
export function getweekOneDay(year: number, month: number) {
  let date = new Date(year, month - 1, 1);
  return date.getDay();
}
/**
 * 获取某年某月的最后一天
 * @param year 
 * @param month 
 * @returns 
 */
export function getMonthLastDay(year: number, month: number) {
  let date = new Date(year, month, 0);
  return date.getDate();
}
/**
 * 返回时间戳的年月日
 * @param timeStamp 时间戳
 * @returns 
 */
export function getDateInfo(timeStamp?: Date) {
  let date = timeStamp ? new Date(timeStamp) : new Date();
  return [
    date.getFullYear(),
    date.getMonth() + 1,
    date.getDate()
  ]
}
export function addZero(time: number) {
  return time >= 10 ? time : `0${time}`;
}
/**
 * 获取时间戳转换剩余天数
 * @param timeStamp 时间戳
 * @returns 
 */
export function getRestTime (timeStamp: number) {
  if(typeof timeStamp !== 'number' || timeStamp <= 0) return;


  const GET_DAY = 1000 * 60 * 60 * 24;
  const GET_HOUR = 1000 * 60 * 60;
  const GET_MINTE = 1000 * 60;
  const GET_SECOND = 1000;

  let day = parseInt(String(timeStamp / GET_DAY));
  let hour = parseInt(String((timeStamp % GET_DAY) / GET_HOUR));
  let minte = parseInt(String((timeStamp % GET_HOUR) / GET_MINTE)); 
  let second = parseInt(String((timeStamp % GET_MINTE) / GET_SECOND));
  let time = `${addZero(day)} 天 ${addZero(hour)} 时 ${addZero(minte)} 分 ${addZero(second)} 秒`;

  return {
    day,
    hour,
    minte,
    second,
    time
  }
}