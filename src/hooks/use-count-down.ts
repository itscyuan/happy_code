import { useEffect, useRef, useState } from "react";
import { getRestTime } from "../utils/dateTools";

export function useCountDown(
  time: Date | string
) {
  let timer = typeof time === 'string' ? new Date(time) : time;

  const timeRef = useRef<any>();

  const [restTime, setRestTime] = useState<string>('');
  
  const getGapTime = () => {
    const curDateMs = new Date().getTime();
    return timer.getTime() - curDateMs;
  }

  useEffect(() => {
    if(timeRef.current) clearInterval(timeRef.current);
    timeRef.current = setInterval(() => {
      let gap = getGapTime();
      
      const { time } = getRestTime(gap) || {};
      if(!time) {
        clearTimeout(timeRef.current);
      }
      setRestTime(time || '');
    }, 1000);
  }, []);
  
  return [restTime];
}