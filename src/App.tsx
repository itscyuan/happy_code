import React, { useState } from 'react';
import { Calendar } from './components/Calendar/Calendar';
import { OverLay } from './components/OverLay/OverLay';
import { useCountDown } from './hooks/use-count-down';
import { Popup } from './components/Popup/Popup';
import { Tabs } from './components/Tabs/Tabs';
import { Toast } from './components/Toast/Toast';
import { LuckyDraw } from './components/LuckyDraw/LuckyDraw';
import { Search } from './components/Search/Search';

function App() {

  const [isVisable, setIsVisable] = useState<boolean>(false);

  // const [time] = useCountDown('2022-12-07 18:00:00');
  const confirm = () => {
    setIsVisable(!isVisable); 
    // Toast.show('我是toast啊');
  }
  const onClicked = () => {
    setIsVisable(false);
  }
  const confirmDate = (data: any) => {
    console.log(data);
    // setIsVisable(false);
  }
  let data = [
    {name: '商品', id: '1'}, 
    {name: '酒店', id: '2'},
    {name: '餐饮', id: '3'},
    {name: '物品', id: '4', disable: true},
    {name: '物品', id: '5'},
    {name: '物品', id: '6'},
    {name: '物品', id: '7'},
    {name: '物品', id: '8'},
    {name: '物品', id: '9'},
    {name: '物品', id: '10'},
    {name: '物品', id: '11'},
    {name: '物品', id: '12'},
    {name: '物品', id: '13'},
    {name: '物品', id: '14'},
    {name: '物品', id: '15'},
    {name: '物品', id: '16'}
  ]
  let prizeData = [
    {prizeOrder: 0, desc: '奖品', id: '0'},
    {prizeOrder: 1, desc: '奖品', id: '1'},
    {prizeOrder: 2, desc: '奖品', id: '2'},
    {prizeOrder: 7, desc: '奖品', id: '3'},
    {prizeOrder: 8, desc: '抽奖', id: '4'},
    {prizeOrder: 3, desc: '奖品', id: '5'},
    {prizeOrder: 6, desc: '奖品', id: '6'},
    {prizeOrder: 5, desc: '奖品', id: '7'},
    {prizeOrder: 4, desc: '奖品', id: '8'}
  ]
  const dataer = [
    {id: "1", content: '我是你哈'},
    {id: "2", content: '我是'},
    {id: "3", content: '什么'},
    {id: "4", content: '你在干什么啊'}
  ]
  return (
    <div className="App">
      <header className="App-header">
        <h1>hello react</h1>
      </header>
      {/* <Tabs 
        tabsData={data}
        // onClicked={confirmDate}
        scrollLeft
      /> */}
      {/* <h1>
        下班倒计时：
        <div>{time}</div>
      </h1> */}
      {/* <Calendar
        defaultValue={['2022-12-15', '2022-12-19']}
        type='range'
        confirm={confirmDate}
        title='日历'
        cancel={onClicked}
        isVisable={isVisable}
      /> */}
      {/* <OverLay
        isVisable={isVisable}
        onClick={onClicked}
      /> */}
      {/* <button onClick={confirm}>open-date</button> */}
      {/* <LuckyDraw /> */}
      <Search listData={dataer} />
    </div>
  );
}

export default App;
